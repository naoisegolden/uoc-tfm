# Account Aggregator

M0.659 - Treball final de màster DADM - Universitat Oberta de Catalunya

Author: Naoise Golden Santos <naoise@uoc.edu>

## How to run

1. Clone or download this repository.
2. Open the Xcode project `AccountAggregator.xcodeproj`.
3. In the project settings, in Signing & Capabilities, add a valid team.
4. Select a device or simulator, preferably an iPhone 13 with iOS 15.4.
5. Run (<kbd>Cmd</kbd> + <kbd>R</kbd>)

## How to use

It's a simple app. 

It contains two main screens: the account list screen and the account detail screen.

When started for the first time, the account list screen will be empty. Tap "Add custom" to add a first custom account. A sheet with a form will appear. Introduce a name and a balance and tap "Create new account".

To connect an external account, in the account list screen, tap "Connect". Select an institution from the list of which you have a valid account and tap "Connect to …". Go through the necessary steps as described during the process with the external provider and your bank. Those steps will involve loging in to your bank account and accepting Nordigen to access your data. Your data is not shared.

Once the process has finalised successfully, a list of your account will appear. Tap to select the ones and tap "Add … accounts". Your newly connected account will appear in the accounts list.

Swipe right on any account to update the balance. If it's a connected account, it will fetch the balance automatically. If it's a manual account, it will show a form to input the new balance. In any case, tap "Update balance" when done.

Tap on an account in the accounts list to navigate to the account details. From this screen, swipe down on the balances list to update. The process is the same as described before. Tap delete if you wish to delete that account and all the balances associated with it. Tap "Back" or swipe left to go back to the accounts list.

That's it!

## License

The MIT License (MIT)
Copyright © 2022 Naoise Golden Santos

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
