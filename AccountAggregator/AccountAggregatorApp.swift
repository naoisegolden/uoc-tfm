//
//  AccountAggregatorApp.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 11/19/21.
//

import SwiftUI

@main
struct AccountAggregatorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
