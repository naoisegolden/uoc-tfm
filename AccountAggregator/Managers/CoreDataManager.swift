//
//  CoreDataManager.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 5/1/22.
//

import Foundation
import CoreData

class CoreDataManager {
    let persistentContainer: NSPersistentContainer
    static let shared = CoreDataManager()
    
    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }

    private init() {
        persistentContainer = NSPersistentContainer(name: "Model")
        persistentContainer.loadPersistentStores { description, error in
            if let error = error {
                fatalError("Unable to initialize Core Data model: \(error)")
            }
        }

        // Use this for inspecting the Core Data
        //if let directoryLocation = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).last {
        //    print("Documents Directory: \(directoryLocation)Application Support")
        //}
    }

    func save() {
        do {
            try viewContext.save()
        } catch {
            viewContext.rollback()
            print(error.localizedDescription)
        }
    }

    func get(id: NSManagedObjectID) -> Account? {
        do {
            return try viewContext.existingObject(with: id) as? Account
        } catch {
            return nil
        }
    }
    
    func getAccount(with nordigenId: String) -> Account? {
        let fetchRequest: NSFetchRequest<Account>
        fetchRequest = Account.fetchRequest()

        fetchRequest.predicate = NSPredicate(
            format: "nordigenId LIKE %@", nordigenId
        )
        
        do {
            return try viewContext.fetch(fetchRequest).first
        } catch {
            return nil
        }
    }

    func list() -> [Account] {
        let request: NSFetchRequest<Account> = Account.fetchRequest()
        
        do {
            return try viewContext.fetch(request)
        } catch {
            print(error)
            return []
        }
    }

    func delete(_ object: NSManagedObject) {
        viewContext.delete(object)
        save()
    }
    
    func update(account: Account, balance: Double) {
        do {
            try viewContext.existingObject(with: account.objectID).setValue(balance, forKey: "balance")
            save()
        } catch {
            // do nothing
        }
    }
    
    func newEntry(account: Account, balance: Double) {
        do {
            let existingAccount = try viewContext.existingObject(with: account.objectID) as! Account
            existingAccount.balance = balance
            existingAccount.modifiedAt = Date.now

            let entry = AccountBalanceEntry(context: viewContext)
            entry.balance = balance
            entry.createdAt = Date.now
            entry.account = existingAccount
            
            save()
        } catch {
            // do nothing
        }
    }

    func getEntry(id: NSManagedObjectID) -> AccountBalanceEntry? {
        do {
            return try viewContext.existingObject(with: id) as? AccountBalanceEntry
        } catch {
            return nil
        }
    }
}
