//
//  Double+toCurrency.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 4/30/22.
//

import Foundation

extension Double {
    var toCurrency:String {
        let number = self as NSNumber
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        return formatter.string(from: number) ?? "NaN"
    }

    func toCurrency(_ currencyCode: String? = nil) -> String {
        let number = self as NSNumber
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = currencyCode
        return formatter.string(from: number) ?? self.toCurrency
    }
}
