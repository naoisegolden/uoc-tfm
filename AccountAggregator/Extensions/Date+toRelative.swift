//
//  Date+toRelative.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 5/30/22.
//

import Foundation

extension Date {
    static let relativaFormatter = RelativeDateTimeFormatter()

    var toRelative: String {
        return Self.relativaFormatter.string(for: self) ?? String(describing: self)
    }
}
