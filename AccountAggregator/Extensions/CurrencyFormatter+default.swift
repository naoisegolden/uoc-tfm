//
//  CurrencyFormatter+default.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 5/30/22.
//

import Foundation
import CurrencyFormatter

extension CurrencyFormatter {
    static let `default`: CurrencyFormatter = {
        .init {
            $0.currency = .euro
            $0.hasDecimals = true
        }
    }()
}
