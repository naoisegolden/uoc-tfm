//
//  AccountViewModel.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 4/30/22.
//

import Foundation
import CoreData

class AccountsListViewModel: ObservableObject {
    
    @Published var accounts = [Account]()

    var totalBalance: Double {
        self.accounts.map { $0.balance }.reduce(0, +)
    }
    
    init() {
        do {
            try PSD2Service.shared.authenticate()
        } catch {
            print(error)
        }
    }
}

extension AccountsListViewModel {
    func list() {
        self.accounts = CoreDataManager.shared.list()
    }
}

extension AccountsListViewModel {
    func delete(_ accountId: NSManagedObjectID) {
        let existingAccount = CoreDataManager.shared.get(id: accountId)
        if let existingAccount = existingAccount {
            CoreDataManager.shared.delete(existingAccount)
        }
    }
}
