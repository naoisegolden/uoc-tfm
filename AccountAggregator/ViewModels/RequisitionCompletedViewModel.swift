//
//  RequisitionCompletedViewModel.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 5/28/22.
//

import Foundation

struct AccountDTO: Identifiable, Hashable {
    var id: String
    var name: String?
    var iban: String? = ""
    var balance: String = ""
    var currency: String = ""
    var referenceDate: Date = Date.now
}

let StatusDictionary = [
    NordigenRequisitionShortStatusType.expired: "The requisition is expired",
    NordigenRequisitionShortStatusType.rejected: "The requisition is rejected",
    NordigenRequisitionShortStatusType.undergoingAuthentication: "The requisition is undergoing authentication",
]

class RequisitionCompletedViewModel: ObservableObject {
    @Published private(set) var loading = false
    @Published private(set) var accounts: [AccountDTO] = []
    @Published private(set) var connectedAccounts: [AccountDTO] = []
    @Published private(set) var status: String? = nil
    @Published private(set) var errorMessage: String? = nil
    
    @Published var selectedAccounts = Set<String>()

    var requisitionId: String?
    var institutionLogo: String?
    var institutionName: String

    init(_ requisition: RequisitionResponseDTO) {
        self.requisitionId = requisition.requisitionId
        self.institutionName = requisition.institutionName
        self.institutionLogo = requisition.institutionLogo
    }
    
    func fetch() {
        guard let id = requisitionId else {
            return
        }

        self.loading = true
    
        PSD2Service.shared.getRequisition(id) { result in
            switch result {
            case .success(let requisition):
                DispatchQueue.main.async {
                    self.status = StatusDictionary[requisition.status] ?? nil
                }
                let dispatchGroup = DispatchGroup()
                requisition.accounts.forEach { account in
                    dispatchGroup.enter()
                    
                    guard let account = account else {
                        defer { dispatchGroup.leave() }

                        return
                    }
                    
                    // Return if account is already connected
                    let fetchedAccount = CoreDataManager.shared.getAccount(with: account)
                    if fetchedAccount != nil {
                        defer { dispatchGroup.leave() }
                        
                        let fetchedAccount = fetchedAccount!
                        let accountDTO = AccountDTO(id: fetchedAccount.nordigenId!, name: fetchedAccount.name, iban: fetchedAccount.iban!, balance: String(fetchedAccount.balance), currency: fetchedAccount.currency!)
                        self.connectedAccounts.append(accountDTO)

                        return
                    }

                    // Get account details (name, IBAN)
                    PSD2Service.shared.getAccountDetails(account) { result in
                        switch result {
                        case .success(let accountDetail):
                            var accountDTO = AccountDTO(id: account)

                            accountDTO.name = accountDetail.account.name
                            accountDTO.iban = accountDetail.account.iban
                            
                            // TODO: Use Combine (or other) to avoid nesting hell
                            // Get account balance
                            PSD2Service.shared.getAccountBalances(account) { result in
                                defer { dispatchGroup.leave() }

                                switch result {
                                case .success(let balances):
                                    guard let balance = balances.balances.first! else {
                                        return
                                    }

                                    accountDTO.balance = balance.balanceAmount.amount
                                    accountDTO.currency = balance.balanceAmount.currency
                                    accountDTO.referenceDate = balance.referenceDate?.toDate ?? Date.now
                                    
                                    self.accounts.append(accountDTO)
                                case .failure(let error):
                                    print(error.localizedDescription)
                                }
                            }
                        case .failure(let error):
                            print(error.localizedDescription)
                        }
                    }
                }

                dispatchGroup.notify(queue: DispatchQueue.main) { [weak self] in
                    self?.loading = false
                }
            case .failure(let error):
                self.loading = false
                self.errorMessage = error.localizedDescription
                print(error.localizedDescription)
            }
        }
    }
    
    func submit() -> Bool {
        self.accounts.filter({ self.selectedAccounts.contains($0.id) }).forEach { account in
            let context = CoreDataManager.shared.viewContext
            let persistedAccount = Account(context: context)
            
            let balance = Double(account.balance) ?? 0.0
            let date = account.referenceDate

            persistedAccount.nordigenId = account.id
            persistedAccount.name = account.name
            persistedAccount.iban = account.iban
            persistedAccount.balance = balance
            persistedAccount.currency = account.currency
            persistedAccount.modifiedAt = date
            persistedAccount.institutionName = self.institutionName
            persistedAccount.logo = self.institutionLogo
            
            CoreDataManager.shared.save()

            // First entry
            CoreDataManager.shared.newEntry(account: persistedAccount, balance: balance)
        }
        
        return true
    }
}
