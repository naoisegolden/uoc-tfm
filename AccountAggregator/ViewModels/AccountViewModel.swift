//
//  AccountViewModel.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 5/1/22.
//

import Foundation
import CoreData

class AccountViewModel: ObservableObject, Identifiable {
    @Published var entries: [AccountBalanceEntry] = []

    var account: Account

    init(_ id: NSManagedObjectID) {
        self.account = CoreDataManager.shared.get(id: id)!
        self.fetchEntries()
    }
    
    var name: String? {
        return account.name
    }
    
    var institution: String? {
        return account.institutionName
    }
    
    var balance: Double {
        return account.balance
    }
    
    var currency: String? {
        return account.currency
    }

    var dateUpdated: Date {
        return account.modifiedAt!
    }
    
    var logo: String {
        return account.logo ?? "default-logo"
    }
    
    var data: [(String, Double)] {
        return Array(self.entries.map({ return ($0.balance.toCurrency, $0.balance) }).reversed())
    }
    
    func fetchEntries() {
        guard let entries = account.entries,
              let myEntries = (entries.allObjects as? [AccountBalanceEntry])
        else {
            self.entries = []
            return
        }

        self.entries = myEntries.sorted(by: { $0.createdAt!.compare($1.createdAt!) == .orderedDescending })
    }

    func delete() {
        let existingAccount = CoreDataManager.shared.get(id: self.account.objectID)
        if let existingAccount = existingAccount {
            CoreDataManager.shared.delete(existingAccount)
        }
    }
    
    func deleteEntry(at offsets: IndexSet) {
        offsets.forEach { index in
            let id = self.entries[index].objectID
            let existingEntry = CoreDataManager.shared.getEntry(id: id)
            if let existingEntry = existingEntry {
                CoreDataManager.shared.delete(existingEntry)
            }
        }
    }
}
