//
//  ConnectInstitutionViewModel.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 5/22/22.
//

import Foundation
import SwiftUI

struct RequisitionResponseDTO {
    var institutionName: String = ""
    var institutionLogo: String? = nil
    let requisitionId: String
    let error: String?
}

struct NordigenInstitutionModel: Decodable, Hashable, Identifiable {
    var id: String
    var name: String
    var logo: String
}

let ErrorDictionary = [
    "canceled_consent_step": "Consent step was canceled"
]

class ConnectInstitutionViewModel: ObservableObject {
    @Published var selectedInstitutionId = ""
    @Published private(set) var errorMessage: String? = nil
    @Published var showErrorMessage = false {
        didSet {
            if showErrorMessage == false {
                // Alert is closed
                self.errorMessage = nil
            }
        }
    }
    @Published var navigate = false

    var requisition: RequisitionResponseDTO? = nil {
        didSet {
            requisition!.institutionName = self.name
            requisition!.institutionLogo = self.logo
            
            guard let error = requisition?.error else {
                self.navigate = true
                return
            }

            self.errorMessage = ErrorDictionary[error, default: error]
            self.showErrorMessage = true
        }
    }

    private(set) var availableInstitutions: [NordigenInstitutionModel] = []
    
    var name: String {
        availableInstitutions.first(where: { $0.id == selectedInstitutionId })?.name ?? ""
    }
    
    var logo: String? {
        availableInstitutions.first(where: { $0.id == selectedInstitutionId })?.logo
    }

    init() {
        PSD2Service.shared.getAvailableInstitutions() { result in
            switch result {
            case .success(let institutions):
                self.availableInstitutions = institutions
            case .failure(let error):
                self.errorMessage = error.localizedDescription
                print(error.localizedDescription)
            }
        }
    }
}

extension ConnectInstitutionViewModel {
    func connectInstitution() {
        let institutionId = self.selectedInstitutionId

        if institutionId.isEmpty {
            return
        }

        PSD2Service.shared.getRequisitionLink(institutionId) { result in
            switch result {
            case .success(let url):
                DispatchQueue.main.async {
                    UIApplication.shared.open(url)
                }
            case .failure(let error):
                self.errorMessage = error.localizedDescription
                print(error.localizedDescription)
            }
        }
    }
}
