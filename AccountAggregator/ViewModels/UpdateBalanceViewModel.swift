//
//  UpdateBalanceViewModel.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 4/30/22.
//

import Foundation

class UpdateBalanceViewModel: ObservableObject {
    @Published var balance: String? = ""
    @Published var balanceFormatted: String = ""
    @Published var balanceDouble: Double? = 0.0
    @Published var balanceDate: Date = Date.now
    @Published var errorMessage: String = ""
    @Published var isFetching = false
    private var account: Account
    
    init(_ account: Account) {
        self.account = account

        if self.isConnected {
            self.isFetching = true
            PSD2Service.shared.getAccountBalances(self.account.nordigenId!) { result in
                DispatchQueue.main.async {
                    self.isFetching = false
                }

                switch result {
                case .success(let balances):
                    guard let balance = balances.balances.first! else {
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.balance = balance.balanceAmount.amount
                        self.balanceFormatted = balance.balanceAmount.amount // Needed to display in CurrencyTextField
                        self.balanceDate = balance.referenceDate?.toDate ?? Date.now
                    }

                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    var previousBalance: Double {
        return account.balance
    }
    
    var previousBalanceDate: Date {
        // TODO: `modifiedAt` is not optional in Core Data, Xcode should not be marking it as optional
        return account.modifiedAt!
    }
    
    var name: String {
        return account.name ?? account.institutionName ?? "Unknown"
    }
    
    var currency: String? {
        return account.currency
    }
    
    var isConnected: Bool {
        return account.nordigenId != nil
    }
}

extension UpdateBalanceViewModel {
    private var isBalanceValid: Bool {
        guard let balanceDouble = balanceDouble else {
            return false
        }

        return balanceDouble >= 0.0
    }

    private func isValid() -> Bool {
        var errors = [String]()

        if !isBalanceValid { errors.append("Balance is not valid") }

        if !errors.isEmpty {
            DispatchQueue.main.async {
                self.errorMessage = errors.joined(separator: "\n")
            }
            return false
        }

        return true
    }
}

extension UpdateBalanceViewModel {
    func update() -> Bool {
        if !isValid() { return false }

        let existingAccount = CoreDataManager.shared.get(id: self.account.objectID)
        if let existingAccount = existingAccount {
            CoreDataManager.shared.update(account: existingAccount, balance: self.balanceDouble!)
            CoreDataManager.shared.newEntry(account: existingAccount, balance: self.balanceDouble!)
        }

        return true
    }
}
