//
//  AddAccountViewModel.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 4/30/22.
//

import Foundation
import CoreData
import CurrencyFormatter

private let availableCurrencies = [
    Currency.euro.rawValue,
    Currency.dollar.rawValue,
    Currency.poundSterling.rawValue,
]

class AddAccountViewModel: ObservableObject {
    @Published var accountName: String = ""
    @Published var institutionName: String = ""
    @Published var balance: String? = ""
    @Published var balanceFormatted: String = ""
    @Published var balanceDouble: Double? = 0.0
    @Published var currency: String = availableCurrencies.first!
    @Published var logo: String? = nil
    @Published var errorMessage: String = ""

    let currencies = availableCurrencies
}

extension AddAccountViewModel {
    private var isAccountNameValid: Bool {
        !self.accountName.isEmpty
    }

    private var isInstitutionNameValid: Bool {
        !self.institutionName.isEmpty
    }
    
    private var isBalanceValid: Bool {
        guard let balanceDouble = balanceDouble else {
            return false
        }
        
        return balanceDouble >= 0.0
    }
    
    private func isValid() -> Bool {
        var errors = [String]()
        
        if !isInstitutionNameValid { errors.append("Missing institution name") }
        if !isBalanceValid { errors.append("Balance is not valid") }
        
        if !errors.isEmpty {
            DispatchQueue.main.async {
                self.errorMessage = errors.joined(separator: "\n")
            }
            return false
        }
        
        return true
    }
}

extension AddAccountViewModel {
    func createAccount() -> Bool {
        if !isValid() { return false }
        
        let context = CoreDataManager.shared.viewContext
        let date = Date.now

        let account = Account(context: context)
        account.institutionName = institutionName
        account.name = accountName.isEmpty ? nil : accountName
        account.balance = balanceDouble!
        account.currency = currency
        account.modifiedAt = date
        account.logo = logo

        CoreDataManager.shared.save()
        
        // First entry
        CoreDataManager.shared.newEntry(account: account, balance: balanceDouble!)
        
        return true
    }
}
