//
//  AsyncLogoView.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 5/22/22.
//

import SwiftUI

struct AsyncLogoView: View {
    var url: String?
    var body: some View {
        VStack {
            if url == nil {
                Image("default-logo").resizable()
            } else {
                AsyncImage(url: URL(string: url!)) { phase in
                    switch phase {
                    case .empty:
                        ProgressView()
                    case .success(let image):
                        image.resizable()
                    case .failure:
                        Image("default-logo").resizable()
                    @unknown default:
                        // Since the AsyncImagePhase enum isn't frozen,
                        // we need to add this currently unused fallback
                        // to handle any new cases that might be added
                        // in the future:
                        EmptyView()
                    }
                }
            }
        }
        .scaledToFit()
        .frame(width: 48.0, height: 48.0, alignment: .center)
        .mask(Circle())
        .padding(.trailing)
        .shadow(color: Color(white: 0, opacity: 0.33), radius: 2, x: 1, y: 1)
    }
}

struct AsyncLogoView_Previews: PreviewProvider {
    static var previews: some View {
        AsyncLogoView(url: "https://cdn.nordigen.com/ais/CAIXABANK_CAIXESBB.png")
    }
}
