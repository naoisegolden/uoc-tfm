//
//  ContentView.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 11/19/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        AccountsScreen()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
