//
//  AccountsList.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 4/30/22.
//

import SwiftUI

struct AccountsScreen: View {
    @ObservedObject private var accountsListVM = AccountsListViewModel()
    @State private var connectAccountScreenIsPresented: Bool = false
    @State private var addAccountScreenIsPresented: Bool = false

    func reloadList() {
        // NOTE: this is mutating accountsListVM.accounts every time
        accountsListVM.list()
    }

    var body: some View {
        NavigationView {
            GeometryReader { metrics in
                ZStack {
                    AccountsListView(accountsListVM.accounts, onDismiss: reloadList)
                        .listStyle(.plain)
                    Circle()
                        .fill(Color("HighlightColor"))
                        .offset(x: 0, y: metrics.size.height - metrics.size.width/1.4)
                        .frame(width: metrics.size.width, height: metrics.size.width)
                    VStack {
                        Text("Total balance:")
                            .font(.title2)
                        Text(accountsListVM.totalBalance.toCurrency("eur"))
                            .font(.title)
                            .fontWeight(.bold)
                    }
                    .offset(x: 0, y: metrics.size.height - metrics.size.width)
                }
                .background(Color("BackgroundColor"))
            }
            .navigationBarItems(
                leading: Button("Connect") {
                    self.connectAccountScreenIsPresented = true
                },
                trailing: Button("Add custom") {
                    self.addAccountScreenIsPresented = true
                })
            .sheet(isPresented: $connectAccountScreenIsPresented) {
                reloadList()
            } content: {
                ConnectInstitutionScreen()
            }
            .sheet(isPresented: $addAccountScreenIsPresented) {
                reloadList()
            } content: {
                AddAccountScreen()
            }
        }
        .onAppear {
            reloadList()
        }
        .navigationViewStyle(.stack) // Fixes weird warning https://stackoverflow.com/a/68019546
    }
}

struct AccountsList_Previews: PreviewProvider {
    static var previews: some View {
        AccountsScreen()
    }
}
