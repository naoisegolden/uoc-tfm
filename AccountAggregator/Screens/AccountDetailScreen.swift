//
//  AccountDetailScreen.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 4/30/22.
//

import SwiftUI
import SwiftUICharts

struct AccountDetailScreen: View {
    @ObservedObject var accountVM: AccountViewModel
    @State private var updateSheetIsPresented: Bool = false
    @State private var deleteAlertIsShown: Bool = false

    @Environment(\.presentationMode) var presentationMode

    static let formatter = RelativeDateTimeFormatter()
    
    init(accountVM: AccountViewModel) {
        self.accountVM = accountVM
    }

    init(_ account: Account) {
        self.accountVM = AccountViewModel(account.objectID)
    }

    func onUpdate() {
        updateSheetIsPresented = true
    }

    func onDelete() {
        deleteAlertIsShown = true
    }
    
    func delete() {
        accountVM.delete()
        presentationMode.wrappedValue.dismiss()
    }
    
    func deleteEntry(at offsets: IndexSet) {
        accountVM.deleteEntry(at: offsets)
        accountVM.fetchEntries()
    }

    var body: some View {
        GeometryReader { geometry in
            VStack {
                ZStack(alignment: .top) {
                    BarChart()
                        .data(accountVM.data)
                        .chartStyle(ChartStyle(backgroundColor: .white,
                                               foregroundColor: [ColorGradient(Color("BackgroundColor"), Color("HighlightColor"))]))
                        .padding(.horizontal)
                        .opacity(0.5)
                    VStack {
                        HStack {
                            AsyncLogoView(url: accountVM.logo)
                            VStack(alignment: .leading) {
                                if let institution = accountVM.institution {
                                    Text(institution).font(.title)
                                    if accountVM.name != nil { Text(accountVM.name!).font(.footnote) }
                                } else {
                                    Text(accountVM.name ?? "No name").font(.title)
                                }
                            }
                        }
                        Text(accountVM.balance.toCurrency(accountVM.currency))
                            .font(.title)
                            .foregroundColor(Color.green)
                    }
                    .shadow(color: .white, radius: 5)
                    .shadow(color: .white, radius: 10)
                }
                .frame(width: geometry.size.width, height: geometry.size.height/3)
                List {
                    ForEach(accountVM.entries) { entry in
                        HStack{
                            Text(entry.balance.toCurrency(accountVM.currency))
                            Spacer()
                            if entry.createdAt != nil { Text(entry.createdAt!, formatter: Self.formatter) }
                        }
                    }
                    .onDelete(perform: deleteEntry)
                }
                .listStyle(.plain)
                .refreshable {
                    onUpdate()
                }
                Spacer()
                HStack {
                    Spacer()
                    Button(role: .destructive, action: onDelete) {
                        Label("Delete", systemImage: "trash")
                    }
                }
                .padding()
            }
        }
        .sheet(isPresented: $updateSheetIsPresented) {
            accountVM.fetchEntries()
        } content: {
            UpdateBalanceScreen(accountVM.account)
        }
        .alert("Are you sure?", isPresented: $deleteAlertIsShown) {
            Button("Cancel", role: .cancel) { }
            Button("Delete account forever", role: .destructive, action: delete)
        }
    }
}

//struct AccountDetailScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        AccountDetailScreen()
//    }
//}
