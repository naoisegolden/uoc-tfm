//
//  ConnectInstitutionScreen.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 5/22/22.
//

import SwiftUI

struct ConnectInstitutionScreen: View {
    @ObservedObject private var vm = ConnectInstitutionViewModel()
    let institutions = ConnectInstitutionViewModel().availableInstitutions
    
    @State var isLoading = false

    private func openRequisition() {
        self.vm.connectInstitution()
        self.isLoading = true
    }
    
    private func handleOpenURL(url: URL) {
        if url.host == "RequisitionCompleted" {
            let queryItems = URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems
            let requisitionId = queryItems?.first(where: { $0.name == "ref" })?.value
            let error = queryItems?.first(where: {$0.name == "error"})?.value

            self.vm.requisition = RequisitionResponseDTO(requisitionId: requisitionId!, error: error)
            
            self.isLoading = false
        }
    }

    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section {
                        Picker("Logo", selection: $vm.selectedInstitutionId) {
                            ForEach(institutions) { institution in
                                HStack {
                                    AsyncLogoView(url: institution.logo)
                                    Text(institution.name).fixedSize()
                                }
                            }
                        }
                        .labelsHidden()
                        .pickerStyle(.inline)
                    }
                }
                .onSubmit {
                    self.openRequisition()
                }
                Spacer()
                HStack {
                    if self.isLoading {
                        ProgressView()
                    }
                    Button("Connect to \(vm.name)") {
                        self.openRequisition()
                    }
                    .padding()
                    .background(Color.accentColor)
                    .foregroundColor(Color.white)
                    .clipShape(Capsule())
                    .disabled(self.vm.selectedInstitutionId.isEmpty)
                    .disabled(self.isLoading)
                }
                Spacer()
                if vm.requisition != nil {
                    NavigationLink(destination: RequisitionCompletedScreen(vm.requisition!), isActive: $vm.navigate) { EmptyView() }
                }
            }
            .alert(self.vm.errorMessage ?? "", isPresented: $vm.showErrorMessage) {
                Button("OK", role: .cancel) {}
            }
            .navigationTitle("Connect institution")
            .onOpenURL(perform: handleOpenURL)
        }
    }
}

struct ConnectInstitutionScreen_Previews: PreviewProvider {
    static var previews: some View {
        ConnectInstitutionScreen()
    }
}
