//
//  RequisitionCompletedScreen.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 5/28/22.
//

import SwiftUI

struct IbanView: View {
    var iban: String
    
    var body: some View {
        let formattedIban = String(iban.enumerated().map { $0 > 0 && $0 % 4 == 0 ? [" ", $1] : [$1]}.joined())
        Text(formattedIban)
    }
}

struct BalanceView: View {
    var balance: String
    var currency: String
    
    var body: some View {
        if let balance = Double(balance) { Text(balance.toCurrency(currency)) }
        else { Text("NaN") }
    }
}

struct AccountSelectionView: View {
    var account: AccountDTO
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(account.name ?? "No name")
                if account.iban != nil {
                    IbanView(iban: account.iban!).font(.footnote)
                }
            }
            Spacer()
            BalanceView(balance: account.balance, currency: account.currency)
                .padding()
        }
    }
}

struct RequisitionCompletedScreen: View {
    @ObservedObject var vm: RequisitionCompletedViewModel
    
    init(_ requisition: RequisitionResponseDTO) {
        self.vm = RequisitionCompletedViewModel(requisition)
    }

    var body: some View {
        NavigationView {
            if self.vm.loading {
                ProgressView()
            } else if (self.vm.errorMessage != nil){
                Text(self.vm.errorMessage ?? "Unknown error")
                    .foregroundColor(Color.red)
            } else {
                VStack {
                    if vm.status != nil { Text(vm.status!).padding().foregroundColor(Color.red) }
                    List(self.vm.accounts, id: \.id, selection: $vm.selectedAccounts) { account in
                        AccountSelectionView(account: account)
                            .tag(account.id)
                    }
                    .overlay {
                        if self.vm.accounts.isEmpty {
                            Text("No new accounts available")
                        }
                    }
                    .environment(\.editMode, Binding.constant(EditMode.active))
                    // TODO: it's not possible to disable individual list items in iOS, making separate list
                    if !self.vm.connectedAccounts.isEmpty {
                        VStack(alignment: .leading) {
                            Text("Already connected:")
                                .frame(maxWidth: .infinity)
                            ForEach(self.vm.connectedAccounts, id: \.id) { account in
                                VStack(alignment: .leading) {
                                    Text(account.name ?? "No name")
                                    Text(account.iban ?? "").font(.footnote)
                                }
                                .padding()
                            }
                        }
                    }
                    Button("Add \(vm.selectedAccounts.count) accounts") {
                        if vm.submit() {
                            // TODO: deprecated in iOS 15. Find way to close 2 open views
                            UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: true)
                        }
                    }
                    .padding()
                    .background(Color.accentColor)
                    .foregroundColor(Color.white)
                    .clipShape(Capsule())
                    .disabled(vm.selectedAccounts.count == 0)
                }
            }
        }
        .toolbar {
            ToolbarItem(placement: .principal) {
                Text(self.vm.institutionName).fixedSize()
            }
        }
        .onAppear {
            self.vm.fetch()
        }
    }
}

struct RequisitionCompletedScreem_Previews: PreviewProvider {
    static var previews: some View {
        let requisition = RequisitionResponseDTO(institutionName: "Sandbox Finance", requisitionId: "", error: nil)
        RequisitionCompletedScreen(requisition)
    }
}
