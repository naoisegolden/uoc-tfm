//
//  UpdateBalanceScreen.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 4/30/22.
//

import SwiftUI
import CurrencyTextField
import CurrencyFormatter

struct UpdateBalanceScreen: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject private var vm: UpdateBalanceViewModel
    @State var hasFocus: Bool? = true

    init(_ account: Account) {
        self.vm = UpdateBalanceViewModel(account)
    }

    private func update() {
        if self.vm.update() {
            self.presentationMode.wrappedValue.dismiss()
        }
    }

    var body: some View {
        NavigationView {
            VStack {
                Text("Previous balance: \(vm.previousBalance.toCurrency(vm.currency))")
                    .fontWeight(.bold)
                Text(vm.previousBalanceDate.toRelative).font(.footnote)
                Spacer()
                VStack {
                    if vm.isFetching {
                        Text("Fetching…")
                        ProgressView()
                    } else {
                        Form {
                            Section(
                                header: Text(vm.isConnected ? "Latest balance" : "Update balance"),
                                footer: Text(vm.isConnected ? "From \(vm.previousBalanceDate.toRelative)" : "")
                            ) {
                                CurrencyTextField(
                                    configuration: .init(
                                        placeholder: 0.toCurrency(vm.currency),
                                        text: $vm.balanceFormatted,
                                        unformattedText: $vm.balance,
                                        inputAmount: $vm.balanceDouble,
                                        // TODO: in the simulator, `hasFocus` breaks input alignment
                                        hasFocus: $hasFocus,
                                        clearsWhenValueIsZero: false,
                                        formatter: vm.currency != nil ? CurrencyFormatter {
                                            $0.currency = Currency(rawValue: vm.currency!)!
                                        } : .default,
                                        textFieldConfiguration: { uiTextField in
                                            uiTextField.font = UIFont.preferredFont(forTextStyle: .title1)
                                            uiTextField.keyboardType = .numberPad
                                            uiTextField.textAlignment = .center
                                        }
                                    )
                                )
                                .disabled(vm.isConnected)
                            }
                        }
                        .onSubmit {
                            self.update()
                        }
                    }
                }
                .frame(maxHeight: .infinity)
                Text(self.vm.errorMessage)
                    .foregroundColor(Color.red)
                Spacer()
                Button("Update balance") {
                    self.update()
                }
                .padding()
                .background(Color.accentColor)
                .foregroundColor(Color.white)
                .clipShape(Capsule())
                Spacer()
            }
            .navigationTitle("Update \(vm.name)")
        }
    }
}

//struct UpdateBalanceScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        UpdateBalanceScreen()
//    }
//}
