//
//  AddAccountScreen.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 4/30/22.
//

import SwiftUI
import CurrencyTextField
import CurrencyFormatter

struct AddAccountScreen: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject private var addAccountVM = AddAccountViewModel()

    private func update() {
        if self.addAccountVM.createAccount() {
            self.presentationMode.wrappedValue.dismiss()
        }
    }

    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section {
                        TextField("Institution name", text: $addAccountVM.institutionName)
                        TextField("Account name (optional)", text: $addAccountVM.accountName)
                        Picker("Currency", selection: $addAccountVM.currency) {
                            ForEach(addAccountVM.currencies, id: \.self) { currency in
                                Text(currency)
                            }
                        }
                    }
                    Section {
                        CurrencyTextField(
                            configuration: .init(
                                placeholder: 0.toCurrency(addAccountVM.currency),
                                text: $addAccountVM.balanceFormatted,
                                unformattedText: $addAccountVM.balance,
                                inputAmount: $addAccountVM.balanceDouble,
                                clearsWhenValueIsZero: false,
                                formatter: CurrencyFormatter {
                                    // For some reason, the `CurrencyTextField` is not updated when $currency changes
                                    $0.currency = Currency(rawValue: addAccountVM.currency)!
                                },
                                textFieldConfiguration: { uiTextField in
                                    uiTextField.font = UIFont.preferredFont(forTextStyle: .title1)
                                    uiTextField.keyboardType = .numberPad
                                    uiTextField.textAlignment = .center
                                }
                            )
                        )
                    }
                }
                .onSubmit {
                    self.update()
                }
                Text(self.addAccountVM.errorMessage).foregroundColor(Color.red)
                Spacer()
                Button("Create new account") {
                    self.update()
                }
                .padding()
                .background(Color.accentColor)
                .foregroundColor(Color.white)
                .clipShape(Capsule())
                Spacer()
            }
            .navigationTitle("Add account")
        }
    }
}

struct AddAccountScreen_Previews: PreviewProvider {
    static var previews: some View {
        AddAccountScreen()
    }
}
