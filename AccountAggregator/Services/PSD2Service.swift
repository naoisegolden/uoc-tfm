//
//  PSD2Service.swift
//  AccountAggregator
//
//  Secrets management taken from https://betterprogramming.pub/manage-secrets-in-your-ios-app-using-xcode-configuration-files-fbceb6e97f47
//
//  Created by Naoise Golden on 5/22/22.
//

import Foundation

enum NetworkError: Error {
    case badUrl
    case codingError
    case decodingError
    case noData
    case underlying(_ error: Error)
}

enum ConfigError: Error {
    case missingKey
    case invalidValue
    case missingToken
    case tokenNotValid
}

enum HttpMethod {
    case get
    case post
}

struct NordigenAuthenticationModel: Decodable {
    let access: String
    let refresh: String
}

enum NordigenRequisitionShortStatusType: String, Decodable {
    case created = "CR"
    case linked = "LN"
    case expired = "EX"
    case rejected = "RJ"
    case undergoingAuthentication = "UA"
    case grantingAccess = "GA"
    case selectingAccounts = "SA"
    case givingConsent = "GC"
}

struct NordigenRequisitionModel: Decodable {
    var id: String
    var link: String
    // NOTE: the documentation is wrong https://nordigen.com/en/docs/account-information/integration/parameters-and-responses/#/requisitions/requisition%20created
    var status: NordigenRequisitionShortStatusType
    var accounts: [String?]
}

/// ```
/// {
///   "account": {
///     "resourceId": "string",
///     "iban": "string",
///     "currency": "string",
///     "ownerName": "string",
///     "name": "string",
///     "product": "string",
///     "cashAccountType": "string"
///   }
/// }
/// ```
struct NordigenAccountDetailsModel: Decodable {
    var account: NordigenAccount
    
    struct NordigenAccount: Decodable {
        var iban: String?
        var name: String?
    }
}

/// ```
/// {
///   "balances": [
///     {
///       "balanceAmount": {
///         "amount": "657.49",
///         "currency": "EUR"
///       },
///       "balanceType": "string",
///       "referenceDate": "2021-11-22"
///     },
///     {
///       "balanceAmount": {
///         "amount": "185.67",
///         "currency": "EUR"
///       },
///       "balanceType": "string",
///       "referenceDate": "2021-11-19"
///     }
///   ]
/// }
///```
struct NordigenBalancesModel: Decodable {
    var balances: [NordigenBalance?]
    
    struct NordigenBalance: Decodable {
        var balanceAmount: NordigenBalanceAmount
        var balanceType: PSD2BalanceType
        var referenceDate: String?
    }
    
    struct NordigenBalanceAmount: Decodable {
        var amount: String
        var currency: String
    }
}

/// Berlin Group PSD2 Balance Types
/// https://www.berlin-group.org/_files/ugd/c2914b_e69952d3a2e247cb8ce1bc767b2946fa.pdf
///
/// `closingBooked`
/// Balance of the account at the end of the pre-agreed account reporting period. It is the sum of the opening booked balance at the beginning of the period and all entries booked to the account during the pre-agreed account reporting period.
///
/// `expected`
/// Balance composed of booked entries and pending items known at the time of calculation, which projects the end of day balance if everything is booked on the account and no other entry is posted.
///
/// `openingBooked`
/// Book balance of the account at the beginning of the account reporting period. It always equals the closing book balance from the previous report.
///
/// `interimAvailable`
/// Available balance calculated in the course of the account servicer's business day, at the time specified, and subject to further changes  during the business day. The interim balance is calculated on the basis of booked credit and debit items during the calculation time/period specified.
///
/// `interimBooked`
/// Balance calculated in the course of the account servicer's business day, at the time specified, and subject to further changes during the business day. The interim balance is calculated on the basis of booked credit and debit items during the calculation time/period specified.
///
/// `forwardAvailable`
/// Forward available balance of money that is at the disposal of the account owner on the date specified.
///
/// `nonInvoiced`
/// Only for card accounts, to be defined yet.
///
enum PSD2BalanceType: String, Decodable {
    case closingBooked = "closingBooked"
    case expected = "expected"
    case openingBooked = "openingBooked"
    case interimAvailable = "interimAvailable"
    case interimBooked = "interimBooked"
    case forwardAvailable = "forwardAvailable"
    case nonInvoiced = "nonInvoiced"
}

fileprivate extension URLRequest {
    func debug() {
        print("\n\(self.httpMethod!) \(self.url!)")
        print("Headers:", self.allHTTPHeaderFields!)
        print("Body:", String(data: self.httpBody ?? Data(), encoding: .utf8)!)
    }
}

class PSD2Service {
    private let apiBaseUrl = "https://ob.nordigen.com/api/v2"
    private var accessToken: String?
    private var refreshToken: String?
    
    static let shared = PSD2Service()
    
    private init() {}
}

extension PSD2Service {
    /// Use Nordigen API Client ID and Key to retrieve and store the authentication token and refresh token.
    func authenticate() throws {
        let endpoint = "/token/new/"
        
        guard let url = URL(string: apiBaseUrl + endpoint) else {
            throw NetworkError.badUrl
        }

        guard let secretId = Bundle.main.object(forInfoDictionaryKey: "NORDIGEN_SECRET_ID") as? String else {
            throw ConfigError.missingKey
        }
        guard let secretKey = Bundle.main.object(forInfoDictionaryKey: "NORDIGEN_SECRET_KEY") as? String else {
            throw ConfigError.missingKey
        }
        let json: [String: Any] = [
            "secret_id": secretId,
            "secret_key": secretKey,
        ]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: json) else {
            throw NetworkError.codingError
        }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        return URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // Request error
                return
            }

            guard let authenticationData = try? JSONDecoder().decode(NordigenAuthenticationModel.self, from: data) else {
                // Decoding error
                print("json:", json)
                print("data:", String(data: data, encoding: .utf8)!)
                return
            }
            
            self.accessToken = authenticationData.access
            self.refreshToken = authenticationData.refresh
        }.resume()
    }
    
    /// Use refresh token to retrieve and store a new authentication token.
    private func reauthenticate() {
        // TODO
    }
}

extension PSD2Service {
    /// Generic call that implements `URLSession.shared.dataTask()` with the appropriate headers and authentication.
    /// Accepts optional `HttpMethod` of type `.get` or `.post` (default is `.get`) and optional `json` data.
    // TODO: is there a better way to implement `URLSession.shared.dataTask` without duplicating the signature?
    private func call(_ endpoint: String, httpMethod: HttpMethod = .get, json: [String: Any]? = nil, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) throws {
        guard let accessToken = accessToken else {
            throw ConfigError.missingToken
        }
        
        var jsonData: Data?
        if (json != nil) {
            jsonData = try JSONSerialization.data(withJSONObject: json!)
        }
        
        guard let url = URL(string: apiBaseUrl + endpoint) else {
            throw NetworkError.badUrl
        }
        
        var request = URLRequest(url: url)

        // TODO: consider deleting for production
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("*/*", forHTTPHeaderField: "Accept")

        if (httpMethod == .post) { request.httpMethod = "POST" }
        if (jsonData != nil) { request.httpBody = jsonData }
        
        // request.debug()

        return URLSession.shared.dataTask(with: request, completionHandler: completionHandler).resume()
    }
}

extension PSD2Service {
    /// Retrieve the requisition link for a given institution
    ///
    /// ```
    /// PSD2Service.shared.getRequisitionLink(institutionId) { result in
    ///     switch result {
    ///         case .success(let url):
    ///         case .failure(let error):
    ///     }
    /// }
    /// ```
    ///
    /// Redirects to `"naoiseuoctfm://RequisitionCompleted"` after  requisition completed
    ///
    func getRequisitionLink(_ institutionId: String, completion: @escaping (Result<URL,NetworkError>) -> Void) {
        let redirectUrl = "naoiseuoctfm://RequisitionCompleted"
        let json: [String: Any] = [
            "redirect": redirectUrl,
            "institution_id": institutionId,
        ]
        
        do {
            try call("/requisitions/", httpMethod: .post, json: json) { data, response, error in
                
                guard let data = data, error == nil else {
                    return completion(.failure(.noData))
                }
                
                guard let requisition = try? JSONDecoder().decode(NordigenRequisitionModel.self, from: data) else {
                    // NOTE: probably bad authentication token
                    return completion(.failure(.decodingError))
                }
                 
                guard let url = URL(string: requisition.link) else {
                    return completion(.failure(.badUrl))
                }
                
                completion(.success(url))
            }
        } catch {
            print(error.localizedDescription)
            completion(.failure(.underlying(error)))
        }
    }
}

extension PSD2Service {
    func getRequisition(_ requisitionId: String, completion: @escaping (Result<NordigenRequisitionModel,NetworkError>) -> Void) {
        do {
            try call("/requisitions/\(requisitionId)/") { data, response, error in
                if error != nil {
                    return completion(.failure(.underlying(error!)))
                }
                
                guard let data = data, error == nil else {
                    return completion(.failure(.noData))
                }
                
                guard let requisition = try? JSONDecoder().decode(NordigenRequisitionModel.self, from: data) else {
                    // NOTE: probably bad authentication token
                    print("data:", String(data: data, encoding: .utf8)!)
                    return completion(.failure(.decodingError))
                }
                
                completion(.success(requisition))
            }
        } catch {
            print(error.localizedDescription)
            completion(.failure(.underlying(error)))
        }
    }
}

extension PSD2Service {
    func getAccountDetails(_ accountId: String, completion: @escaping (Result<NordigenAccountDetailsModel,NetworkError>) -> Void) {
        do {
            try call("/accounts/\(accountId)/details/") { data, response, error in
                if error != nil {
                    return completion(.failure(.underlying(error!)))
                }
                
                guard let data = data, error == nil else {
                    return completion(.failure(.noData))
                }
                
                guard let balances = try? JSONDecoder().decode(NordigenAccountDetailsModel.self, from: data) else {
                    // NOTE: probably bad authentication token
                    print("data:", String(data: data, encoding: .utf8)!)
                    return completion(.failure(.decodingError))
                }
                
                completion(.success(balances))
            }
        } catch {
            print(error.localizedDescription)
            completion(.failure(.underlying(error)))
        }
    }
}

extension PSD2Service {
    func getAccountBalances(_ accountId: String, completion: @escaping (Result<NordigenBalancesModel,NetworkError>) -> Void) {
        do {
            try call("/accounts/\(accountId)/balances/") { data, response, error in
                if error != nil {
                    return completion(.failure(.underlying(error!)))
                }
                
                guard let data = data, error == nil else {
                    return completion(.failure(.noData))
                }
                
                guard let balances = try? JSONDecoder().decode(NordigenBalancesModel.self, from: data) else {
                    // NOTE: probably bad authentication token
                    print("data:", String(data: data, encoding: .utf8)!)
                    return completion(.failure(.decodingError))
                }
                
                completion(.success(balances))
            }
        } catch {
            print(error.localizedDescription)
            completion(.failure(.underlying(error)))
        }
    }
}

extension PSD2Service {
    func getAvailableInstitutions(completion: @escaping (Result<[NordigenInstitutionModel],NetworkError>) -> Void) {
        guard let url = Bundle.main.url(forResource: "institutions-es", withExtension: "json") else {
            return
        }

        do {
            let data = try Data(contentsOf: url)
            let result = try JSONDecoder().decode([NordigenInstitutionModel].self, from: data)

            completion(.success(result))
        } catch {
            completion(.failure(.underlying(error)))
        }
    }
}

extension String {
    var toDate: Date {
        let formatter = DateFormatter()

        formatter.dateFormat = "yyyy-MM-dd"

        return formatter.date(from: self) ?? Date.now
    }
}
