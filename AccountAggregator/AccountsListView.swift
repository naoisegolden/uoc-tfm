//
//  AccountsListView.swift
//  AccountAggregator
//
//  Created by Naoise Golden on 4/30/22.
//

import SwiftUI

struct AccountRow: View {
    var account: Account

    var body: some View {
        HStack {
            AsyncLogoView(url: account.logo)
            VStack(alignment: .leading) {
                if let name = account.institutionName {
                    Text(name).font(.headline)
                    if account.name != nil { Text(account.name!).font(.footnote) }
                } else {
                    Text(account.name ?? "No name").font(.headline)
                }
                Text(account.modifiedAt!.toRelative)
                    .font(.footnote)
                    .foregroundColor(Color.gray)
            }
            Spacer()
            Text(account.balance.toCurrency(account.currency))
        }
        .padding(4.0)
    }
}

struct AccountsListView: View {
    @State private var selectedAccount: Account? = nil

    typealias RefreshClosure = (() -> Void)?

    var accounts: [Account]
    var onDismiss: RefreshClosure
    
    init(_ accounts: [Account], onDismiss: RefreshClosure = nil) {
        self.accounts = accounts
        self.onDismiss = onDismiss
    }

    var body: some View {
        List(accounts, id: \.id) { account in
            NavigationLink(destination: AccountDetailScreen(account)) {
                AccountRow(account: account)
                    .listRowBackground(Color.clear)
            }
            .swipeActions(edge: .leading) {
                Button {
                    selectedAccount = account
                } label: {
                    Label("Update", systemImage: "arrow.clockwise")
                }
                .tint(Color.accentColor)
            }
            .listRowBackground(Color.clear)
            .listRowSeparator(.hidden)
        }
        .sheet(item: $selectedAccount)  {
            onDismiss?()
        } content: { account in
            UpdateBalanceScreen(account)
        }
        .overlay(Group {
            if accounts.isEmpty  {
                VStack {
                    Image("LaunchScreenImage")
                    Text("No accounts yet!")
                }
            }
        })
    }
}

//struct AccountsListView_Previews: PreviewProvider {
//    static var previews: some View {
//        AccountsListView()
//    }
//}
